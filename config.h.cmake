#ifndef CONFIG_H
#define CONFIG_H

// Define the sessionk version.
#cmakedefine APP_VERSION "@APP_VERSION@"

// Define to 1 if you have the `_IceTransNoListen' function.
#cmakedefine HAVE__ICETRANSNOLISTEN 1

#endif //CONFIG_H
