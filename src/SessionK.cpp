/***************************************************************************
 *   Copyright (C) 2013 by Daniel Nicoletti <dantti12@gmail.com>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; see the file COPYING. If not, write to       *
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,  *
 *   Boston, MA 02110-1301, USA.                                           *
 ***************************************************************************/

#include "SessionK.h"

#include "XSMP/server.h"

#include "KDEDLoader.h"
#include "KLauncherInterface.h"
#include "DBusInterface.h"
#include "Environment.h"
#include "ShortcutCollection.h"
#include "WindowListener.h"

#include <QStringBuilder>
#include <QTime>

#include <KLocale>
#include <KCmdLineArgs>
#include <KDebug>

#include <KProcess>
#include <KToolInvocation>

SessionK::SessionK(QObject *parent) :
    QObject(parent),
    m_phase(0),
    m_dbusInterface(new DBusInterface(this)),
    m_shortcutCollection(0)
{
    m_time.start();
}

SessionK::~SessionK()
{
    kWarning() << "Terminating...";
}

bool SessionK::isRegistered() const
{
    return m_dbusInterface->isRegistered();
}

void SessionK::init()
{
    kWarning() << "Init session" << time();
    kWarning() << printProcess("Right after init");

    m_ksmServer = new KSMServer(QString(), false, false);
    connect(m_ksmServer, SIGNAL(resumeStartup()), SLOT(windowManagerAndKDEDPhaseDone()));

    WindowListener *listener = new WindowListener();
    connect(listener, SIGNAL(desktopCreated()), SLOT(shellPhaseDone()));

    m_klauncherInterface = new KLauncherInterface(this);
    connect(m_klauncherInterface, SIGNAL(laucherStarted()), SLOT(nextPhase()));
    connect(m_klauncherInterface, SIGNAL(autoStart0Done()), SLOT(autoStart0Done()));
    connect(m_klauncherInterface, SIGNAL(autoStart1Done()), SLOT(autoStart1Done()));
    connect(m_klauncherInterface, SIGNAL(autoStart2Done()), SLOT(autoStart2Done()));

    kWarning() << printProcess("Right after set launch env");

    bool locked = Environment::global()->contains(QLatin1String("DESKTOP_LOCKED"));
    kWarning() << "Session locked?" << locked;

    m_kdedLoader = new KDEDLoader(this);

    // Link "tmp" "socket" and "cache" resources to directory in /tmp
    // Creates:
    // - a directory /tmp/kde-$USER and links $KDEHOME/tmp-$HOSTNAME to it.
    // - a directory /tmp/ksocket-$USER and links $KDEHOME/socket-$HOSTNAME to it.
    // - a directory /var/tmp/kdecache-$USER and links $KDEHOME/cache-$HOSTNAME to it.
    // Note: temporary locations can be overriden through the KDETMP and KDEVARTMP
    // environment variables
    kWarning() << printProcess("before lnusertemp");

    QStringList resources;
    resources << QLatin1String("tmp")
              << QLatin1String("cache")
              << QLatin1String("socket");
    foreach (const QString &resource, resources) {
        KProcess lnusertemp;
        lnusertemp.setProcessEnvironment(*Environment::global());
        lnusertemp << QLatin1String("lnusertemp") << resource;
        lnusertemp.startDetached();
    }

    KProcess process;
    process.setProcessEnvironment(*Environment::global());
    process << QLatin1String("kdeinit4");
    process << QLatin1String("--no-kded");
    process.startDetached();
    process.waitForFinished();

    kWarning() << printProcess("Inited....") << process.pid();
}

void SessionK::windowManagerAndKDEDPhase()
{
    kWarning() << printProcess("windowManagerAndKDEDPhaseDone after");
    KToolInvocation::kdeinitExec(QLatin1String("kwin"));

    m_shortcutCollection = new ShortcutCollection(this);
    connect(m_shortcutCollection, SIGNAL(logout()), SLOT(logout()));
    connect(m_shortcutCollection, SIGNAL(logoutWithoutConfirmation()), SLOT(logoutWithoutConfirmation()));
    connect(m_shortcutCollection, SIGNAL(haltWithoutConfirmation()), SLOT(haltWithoutConfirmation()));
    connect(m_shortcutCollection, SIGNAL(rebootWithoutConfirmation()), SLOT(rebootWithoutConfirmation()));
}

void SessionK::windowManagerAndKDEDPhaseDone()
{
    kWarning() << printProcess("windowManagerAndKDEDPhaseDone after");

    nextPhase(WindowManagerAndKDEDDone);
}

void SessionK::shellPhase()
{
    kWarning() << printProcess("kdeinit after");


//    KToolInvocation::kdeinitExec(QLatin1String("razor-desktop"));
//    KToolInvocation::kdeinitExec(QLatin1String("razor-panel"));
    KToolInvocation::kdeinitExec(QLatin1String("plasma-desktop"));
}

void SessionK::shellPhaseDone()
{
    kWarning() << printProcess("Right after shellPhaseDone");
    m_phase |= ShellPhaseDone;
    QTimer::singleShot(500, this, SLOT(nextPhase()));
}

void SessionK::autoStart0()
{
    kWarning() << printProcess("Right after autoStart0");
//    KToolInvocation::kdeinitExec(QLatin1String("kded4"));

//    m_kdedLoader->loadModules();

    m_klauncherInterface->autoStart(0);
}

void SessionK::autoStart0Done()
{
    kWarning() << printProcess("Right after autoStart0Done");
//    m_ksmServer->autoStart0Done();

    nextPhase(AutoStartPhase0Done);
}

void SessionK::autoStart1()
{
    m_klauncherInterface->autoStart(1);
}

void SessionK::autoStart1Done()
{
    kWarning() << printProcess("Right after autoStart0Done");
//    m_ksmServer->autoStart1Done();
    nextPhase(AutoStartPhase1Done);
}

void SessionK::autoStart2()
{
    kWarning() << printProcess("Right after autoStart1Done");
    m_klauncherInterface->autoStart(2);
}

void SessionK::autoStart2Done()
{
    kWarning() << printProcess("Right after autoStart2Done");
//    m_ksmServer->autoStart2Done();
    nextPhase(AutoStartPhase2Done);
}

void SessionK::restoreSession()
{
    kDebug();

    kWarning() << "loadmodules" << time();
    kWarning() << time();

    restoreSessionDone();
}

void SessionK::restoreSessionDone()
{
    kWarning() << printProcess("Right after restoreSessionDone");
    nextPhase(RestoreSessionDone);
}

void SessionK::logout()
{
    m_ksmServer->shutdown(KWorkSpace::ShutdownConfirmYes, KWorkSpace::ShutdownTypeDefault, KWorkSpace::ShutdownModeDefault);
}

void SessionK::logoutWithoutConfirmation()
{
    m_ksmServer->shutdown(KWorkSpace::ShutdownConfirmNo, KWorkSpace::ShutdownTypeNone, KWorkSpace::ShutdownModeDefault);
}

void SessionK::haltWithoutConfirmation()
{
    m_ksmServer->shutdown(KWorkSpace::ShutdownConfirmNo, KWorkSpace::ShutdownTypeHalt, KWorkSpace::ShutdownModeDefault);
}

void SessionK::rebootWithoutConfirmation()
{
    m_ksmServer->shutdown(KWorkSpace::ShutdownConfirmNo, KWorkSpace::ShutdownTypeReboot, KWorkSpace::ShutdownModeDefault);
}

void SessionK::nextPhase(Phase phase)
{
    m_phase |= phase;

    if (!(m_phase & WindowManagerAndKDEDDone)) {
        windowManagerAndKDEDPhase();
        return;
    }

    if (!(m_phase & ShellPhaseDone)) {
        shellPhase();
        return;
    }

    if (!(m_phase & AutoStartPhase0Done)) {
        autoStart0();
        return;
    }

    if (!(m_phase & AutoStartPhase1Done)) {
        autoStart1();
        return;
    }

    if (!(m_phase & AutoStartPhase2Done)) {
        autoStart2();
        return;
    }

    if (!(m_phase & RestoreSessionDone)) {
        restoreSession();
        return;
    }

    kDebug() << printProcess("Startup finished.");
}

QString SessionK::printProcess(const QString &text)
{
//    KProcess *ps = new KProcess(this);
//    ps->setOutputChannelMode(KProcess::MergedChannels);
//    *ps << "ps" << "ux";
//    ps->start();
//    ps->waitForFinished();
//    kWarning() << ps->readAll();

    return text % QLatin1Char(' ') % time();
}

QString SessionK::time() const
{
    return QString("%1 ms").arg(m_time.elapsed());
}
