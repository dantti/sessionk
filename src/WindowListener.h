/**
 * @file /src/windows/WindowListener.h
 *
 * This file is part of Touchégg.
 *
 * Touchégg is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License  as  published by  the  Free Software
 * Foundation,  either version 2 of the License,  or (at your option)  any later
 * version.
 *
 * Touchégg is distributed in the hope that it will be useful,  but  WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the  GNU General Public License  for more details.
 *
 * You should have received a copy of the  GNU General Public License along with
 * Touchégg. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author José Expósito <jose.exposito89@gmail.com> (C) 2011 - 2012
 * @author Daniel Nicoletti <dantti12@gmail.com> (C) 2013
 * @class  WindowListener
 */
#ifndef WINDOWLISTENER_H
#define WINDOWLISTENER_H

#include <QWidget>

#include <X11/Xatom.h>

/**
 * Detects the creation or destruction of the windows and emits the windowCreated or windowDeleted signals respectively.
 */
class WindowListenerPrivate;
class WindowListener : public QWidget
{
    Q_OBJECT
public:
    WindowListener();
    ~WindowListener();

    /**
     * Called whenever a X11 event occurs. Is necessary to filter that event to know if is a creation or destruction of
     * a window and emit the corresponding signals.
     */
    bool x11Event(XEvent *event);
    bool hasDesktop() const;

signals:
    void desktopCreated();
    void desktopDestroyed();

private:
    WindowListenerPrivate *d_ptr;
    Q_DECLARE_PRIVATE(WindowListener)
};

#endif // WINDOWLISTENER_H
