/***************************************************************************
 *   Copyright (C) 2013 by Daniel Nicoletti <dantti12@gmail.com>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; see the file COPYING. If not, write to       *
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,  *
 *   Boston, MA 02110-1301, USA.                                           *
 ***************************************************************************/

#include "SessionK.h"
#include <config.h>

#include <KApplication>

#include <KConfig>
#include <KLocale>
#include <KAboutData>
#include <KComponentData>
#include <KGlobal>
#include <KCmdLineArgs>

#include <KDebug>

int main(int argc, char **argv)
{
    KAboutData aboutData("sessionk",
                         "sessionk",
                         ki18n("sessionk"),
                         APP_VERSION,
                         ki18n("sessionk - KDE session daemon"),
                         KAboutData::License_GPL,
                         ki18n("(C) 2013 Daniel Nicoletti"));

    aboutData.addAuthor(ki18n("Daniel Nicoletti"), KLocalizedString(), "dantti12@gmail.com", "http://dantti.wordpress.com");
    KComponentData component(aboutData);
    KGlobal::setActiveComponent(component);

    KCmdLineArgs::init(argc, argv, &aboutData);

    KApplication app;
    app.setQuitOnLastWindowClosed(false);
    kWarning() << "SessionK!" << app.applicationPid();

    SessionK sessionk(&app);
    if (!sessionk.isRegistered()) {
        kWarning() << "SessionK is already running!" << app.applicationPid();
        // parse cmd line
        return 0;
    }

    sessionk.init();
    return app.exec();
}
