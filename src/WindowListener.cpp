/**
 * @file /src/touchegg/windows/WindowListener.cpp
 *
 * This file is part of Touchégg.
 *
 * Touchégg is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License  as  published by  the  Free Software
 * Foundation,  either version 2 of the License,  or (at your option)  any later
 * version.
 *
 * Touchégg is distributed in the hope that it will be useful,  but  WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE.  See the  GNU General Public License  for more details.
 *
 * You should have received a copy of the  GNU General Public License along with
 * Touchégg. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author José Expósito <jose.exposito89@gmail.com> (C) 2011 - 2012
 * @class  WindowListener
 */
#include "WindowListener.h"

#include <QX11Info>

#include <KSystemEventFilter>
#include <NETWinInfo>
#include <netwm_def.h>

#include <KDebug>

// Xlib
#include <X11/Xlib.h>
#include <X11/Xutil.h>

class WindowListenerPrivate
{
public:
    NETRootInfo *rootInfo;
    Window desktop;
};

WindowListener::WindowListener() :
    d_ptr(new WindowListenerPrivate)
{
    Q_D(WindowListener);
    d->rootInfo = new NETRootInfo(QX11Info::display(), NET::ClientList);
    d->desktop = 0;

    KSystemEventFilter::installEventFilter(this);
}

WindowListener::~WindowListener()
{
    Q_D(WindowListener);

    delete d->rootInfo;
    delete d;
}

bool WindowListener::x11Event(XEvent *event)
{
    Q_D(WindowListener);

    unsigned long prop = d->rootInfo->event(event);
    if (prop & NET::ClientList) {
        const Window *clients = d->rootInfo->clientList();
        int clientCount = d->rootInfo->clientListCount();

        if (d->desktop) {
            for (int i = 0; i < clientCount; ++i) {
                if (d->desktop == clients[i]) {
                    return false;
                }
            }
            emit desktopDestroyed();
            d->desktop = 0;
        }

        const unsigned long properties[1] = { NET::WMWindowType };
        for (int i = 0; i < clientCount; ++i) {
            NETWinInfo2 info(QX11Info::display(),
                             clients[i],
                             d->rootInfo->rootWindow(),
                             properties,
                             1);

            if (info.windowType(NET::DesktopMask) == NET::Desktop) {
                d->desktop = clients[i];
                emit desktopCreated();
                break;
            }
        }
    }

    return false;
}

bool WindowListener::hasDesktop() const
{
    Q_D(const WindowListener);

    return d->desktop;
}
