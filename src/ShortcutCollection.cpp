/***************************************************************************
 *   Copyright (C) 2013 by Daniel Nicoletti <dantti12@gmail.com>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; see the file COPYING. If not, write to       *
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,  *
 *   Boston, MA 02110-1301, USA.                                           *
 ***************************************************************************/

#include "ShortcutCollection.h"

#include <KAuthorized>
#include <KAction>
#include <KLocalizedString>
#include <KToolInvocation>

ShortcutCollection::ShortcutCollection(QObject *parent) :
    KActionCollection(parent)
{
    m_initted = !KToolInvocation::kdeinitExec(QLatin1String("kglobalaccel"));
    if (KAuthorized::authorize(QLatin1String("logout"))) {
        KAction *a;
        a = addAction("Log Out");
        a->setText(i18n("Log Out"));
        a->setGlobalShortcut(KShortcut(Qt::CTRL + Qt::ALT + Qt::Key_Delete));
        connect(a, SIGNAL(triggered(bool)), SIGNAL(logout()));

        a = addAction("Log Out (Secondary)");
        a->setText(i18n("Log Out (Secondary)"));
        a->setGlobalShortcut(KShortcut(Qt::META + Qt::SHIFT + Qt::Key_Eject));
        connect(a, SIGNAL(triggered(bool)), SIGNAL(logout()));

        a = addAction("Log Out Without Confirmation");
        a->setText(i18n("Log Out Without Confirmation"));
        a->setGlobalShortcut(KShortcut(Qt::CTRL + Qt::ALT + Qt::SHIFT + Qt::Key_Delete));
        connect(a, SIGNAL(triggered(bool)), SIGNAL(logoutWithoutConfirmation()));

        a = addAction("Log Out Without Confirmation (Secondary)");
        a->setText(i18n("Log Out Without Confirmation (Secondary)"));
        a->setGlobalShortcut(KShortcut(Qt::META + Qt::ALT + Qt::SHIFT + Qt::Key_Q));
        connect(a, SIGNAL(triggered(bool)), SIGNAL(logoutWithoutConfirmation()));

        a = addAction("Halt Without Confirmation");
        a->setText(i18n("Halt Without Confirmation"));
        a->setGlobalShortcut(KShortcut(Qt::CTRL + Qt::ALT + Qt::SHIFT + Qt::Key_PageDown));
        connect(a, SIGNAL(triggered(bool)), SIGNAL(haltWithoutConfirmation()));

        a = addAction("Halt Without Confirmation (Secondary)");
        a->setText(i18n("Halt Without Confirmation (Secondary)"));
        a->setGlobalShortcut(KShortcut(Qt::META + Qt::ALT + Qt::Key_Eject));
        connect(a, SIGNAL(triggered(bool)), SIGNAL(haltWithoutConfirmation()));

        a = addAction("Reboot Without Confirmation");
        a->setText(i18n("Reboot Without Confirmation"));
        a->setGlobalShortcut(KShortcut(Qt::CTRL + Qt::ALT + Qt::SHIFT + Qt::Key_PageUp));
        connect(a, SIGNAL(triggered(bool)), SIGNAL(rebootWithoutConfirmation()));

        a = addAction("Reboot Without Confirmation (Secondary)");
        a->setText(i18n("Reboot Without Confirmation (Secondary)"));
        a->setGlobalShortcut(KShortcut(Qt::META + Qt::CTRL + Qt::Key_Eject));
        connect(a, SIGNAL(triggered(bool)), SIGNAL(rebootWithoutConfirmation()));

        a = addAction("Lock screen");
        a->setText(i18n("Lock screen"));
        a->setGlobalShortcut(KShortcut(Qt::CTRL + Qt::ALT + Qt::Key_L));
        connect(a, SIGNAL(triggered(bool)), SIGNAL(lockScreen()));

        a = addAction("Lock screen (Secondary)");
        a->setText(i18n("Lock screen (Secondary)"));
        a->setGlobalShortcut(KShortcut(Qt::CTRL + Qt::SHIFT + Qt::Key_Eject));
        connect(a, SIGNAL(triggered(bool)), SIGNAL(lockScreen()));
    }
}

bool ShortcutCollection::initted() const
{
    return m_initted;
}
