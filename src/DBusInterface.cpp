/***************************************************************************
 *   Copyright (C) 2013 by Daniel Nicoletti <dantti12@gmail.com>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; see the file COPYING. If not, write to       *
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,  *
 *   Boston, MA 02110-1301, USA.                                           *
 ***************************************************************************/

#include "DBusInterface.h"
#include "sessionkadaptor.h"

#include "Environment.h"

#include <QtDBus/QDBusConnection>

#include <KProcess>
#include <KDebug>

#include <kworkspace/kworkspace.h>

DBusInterface::DBusInterface(QObject *parent) :
    QObject(parent),
    m_registered(true)
{
    if (!Environment::global()->contains(QLatin1String("DBUS_SESSION_BUS_ADDRESS"))) {
        kWarning() << "Launching DBus" << qApp->arguments().first();
        KProcess process;
        process << QLatin1String("dbus-launch")
                << QLatin1String("--exit-with-session")
                << qApp->arguments().first();
        process.start();
        process.waitForFinished(5000);
    }

    (void) new SessionkAdaptor(this);
    if (!QDBusConnection::sessionBus().registerService(QLatin1String("org.kde.sessionk"))) {
        kWarning() << "unable to register service to dbus";
        m_registered = false;
        return;
    }

    if (!QDBusConnection::sessionBus().registerService("org.kde.ksmserver")) {
        kWarning() << "unable to register ksmserver interface to dbus";
        m_registered = false;
        return;
    }

    if (!QDBusConnection::sessionBus().registerObject("/org/kde/sessionk", this)) {
        kWarning() << "unable to register object to dbus";
        m_registered = false;
        return;
    }

//    if (!QDBusConnection::sessionBus().registerService("org.kde.kded")) {
//        kDebug() << "unable to register ksmserver interface to dbus";
//        m_registered = false;
//        return;
//    }
}

DBusInterface::~DBusInterface()
{
}

bool DBusInterface::isRegistered() const
{
    return m_registered;
}
