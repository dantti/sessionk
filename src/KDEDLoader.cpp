/***************************************************************************
 *   Copyright (C) 2013 by Daniel Nicoletti <dantti12@gmail.com>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; see the file COPYING. If not, write to       *
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,  *
 *   Boston, MA 02110-1301, USA.                                           *
 ***************************************************************************/

#include "KDEDLoader.h"

#include <QStringBuilder>
#include <QDBusPendingCall>

#include <KServiceTypeTrader>
#include <KService>
#include <KDesktopFile>
#include <KConfigGroup>

#include <KDebug>

KDEDLoader::KDEDLoader(QObject *parent) :
    QObject(parent),
    m_interface(QLatin1String("org.kde.kded"),
                QLatin1String("/kded"),
                QLatin1String("org.kde.kded"))
{
}

void KDEDLoader::loadModules()
{
    KConfig kdedrc("kdedrc", KConfig::NoGlobals);
    KService::List offers = KServiceTypeTrader::self()->query(QLatin1String("KDEDModule"));
    foreach (const KService::Ptr &service, offers) {
        const KDesktopFile file("services", service->entryPath());
        const KConfigGroup grp = file.desktopGroup();
        kWarning() << "------>" << service->entryPath() << service->name() << grp.readEntry("X-KDE-Library");
        kWarning() << service->property("X-KDE-Kded-autoload", QVariant::Bool).toBool();
        kWarning() << service->property("X-KDE-Kded-phase", QVariant::Int).toInt();

        // The logic has to be identical to Kded::initModules.
        // They interpret X-KDE-Kded-autoload as false if not specified
        //                X-KDE-Kded-load-on-demand as true if not specified
        if (grp.readEntry("X-KDE-Kded-autoload", false) && autoloadEnabled(&kdedrc, file.name())) {
            m_interface.call(QDBus::BlockWithGui, QLatin1String("loadModule"), grp.readEntry("X-KDE-Library"));
        }
    }
}

bool KDEDLoader::autoloadEnabled(KConfig *config, const QString &filename)
{
    KConfigGroup cg(config, setModuleGroup(filename));
    return cg.readEntry("autoload", true);
}

QString KDEDLoader::setModuleGroup(const QString &filename)
{
    QString module = filename;
    int i = module.lastIndexOf(QLatin1Char('/'));
    if (i != -1) {
        module = module.mid(i+1);
    }
    i = module.lastIndexOf(QLatin1Char('.'));
    if (i != -1) {
        module = module.left(i);
    }

    return QLatin1String("Module-") % module;
}
