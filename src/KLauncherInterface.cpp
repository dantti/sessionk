/***************************************************************************
 *   Copyright (C) 2013 by Daniel Nicoletti <dantti12@gmail.com>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; see the file COPYING. If not, write to       *
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,  *
 *   Boston, MA 02110-1301, USA.                                           *
 ***************************************************************************/

#include "KLauncherInterface.h"

#include <klauncher_iface.h>
#include <KUser>

#include <QDBusConnection>

#include <KDebug>

KLauncherInterface::KLauncherInterface(QObject *parent) :
    QObject(parent),
    m_interface(new OrgKdeKLauncherInterface(QLatin1String("org.kde.klauncher"),
                                             QLatin1String("/KLauncher"),
                                             QDBusConnection::sessionBus(),
                                             this)),
    m_watcher(new QDBusServiceWatcher(QLatin1String("org.kde.klauncher"),
                                      QDBusConnection::sessionBus(),
                                      QDBusServiceWatcher::WatchForRegistration,
                                      this))
{
    connect(m_interface, SIGNAL(autoStart0Done()), SIGNAL(autoStart0Done()));
    connect(m_interface, SIGNAL(autoStart1Done()), SIGNAL(autoStart1Done()));
    connect(m_interface, SIGNAL(autoStart2Done()), SIGNAL(autoStart2Done()));
    connect(m_watcher, SIGNAL(serviceRegistered(QString)), SLOT(serviceRegistered()));
}

void KLauncherInterface::autoStart(int phase)
{
    m_interface->autoStart(phase);
}

void KLauncherInterface::serviceRegistered()
{
    kWarning();
    KUser user;
    m_interface->setLaunchEnv(QLatin1String("KDE_FULL_SESSION"),
                              QLatin1String("true"));
    m_interface->setLaunchEnv(QLatin1String("KDE_SESSION_VERSION"),
                              QLatin1String("4"));
    m_interface->setLaunchEnv(QLatin1String("KDE_SESSION_UID"),
                              QString::number(user.uid()));
    emit laucherStarted();
}
