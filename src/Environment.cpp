/***************************************************************************
 *   Copyright (C) 2013 by Daniel Nicoletti <dantti12@gmail.com>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; see the file COPYING. If not, write to       *
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,  *
 *   Boston, MA 02110-1301, USA.                                           *
 ***************************************************************************/

#include "Environment.h"

#include <QCoreApplication>

#include <KUser>

#include <KDebug>

Environment* Environment::m_global = 0;

Environment* Environment::global()
{
    if(!m_global) {
        m_global = new Environment(qApp);
    }

    return m_global;
}

Environment::Environment(QObject *parent) :
    QObject(parent),
    QProcessEnvironment(QProcessEnvironment::systemEnvironment())
{
    m_inited = init();
}

bool Environment::init()
{
    // Check DBus
    if (!contains(QLatin1String("DBUS_SESSION_BUS_ADDRESS"))) {
        kWarning() << "DBus session bus missing";
        return false;
    }

    KUser user;
    insert(QLatin1String("KDE_FULL_SESSION"), QLatin1String("true"));
    insert(QLatin1String("KDE_SESSION_VERSION"), QLatin1String("4"));
    insert(QLatin1String("KDE_SESSION_UID"), QString::number(user.uid()));

    return true;
}
