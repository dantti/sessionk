/***************************************************************************
 *   Copyright (C) 2013 by Daniel Nicoletti <dantti12@gmail.com>           *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; see the file COPYING. If not, write to       *
 *   the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,  *
 *   Boston, MA 02110-1301, USA.                                           *
 ***************************************************************************/

#ifndef SESSIONK_H
#define SESSIONK_H

#include <QObject>
#include <QTime>

class KSMServer;
class KDEDLoader;
class DBusInterface;
class ShortcutCollection;
class KLauncherInterface;
class SessionK : public QObject
{
    Q_OBJECT
    Q_FLAGS(Phase Phases)
public:
    enum Phase {
        KDEInitPhaseDone         = 1 << 0,
        WindowManagerAndKDEDDone = 1 << 1,
        ShellPhaseDone           = 1 << 2,
        AutoStartPhase0Done      = 1 << 3,
        AutoStartPhase1Done      = 1 << 4,
        AutoStartPhase2Done      = 1 << 5,
        RestoreSessionDone       = 1 << 6
    };
    Q_DECLARE_FLAGS(Phases, Phase)
    explicit SessionK(QObject *parent = 0);
    ~SessionK();

    bool isRegistered() const;

    void init();

private slots:
    void windowManagerAndKDEDPhase();
    void windowManagerAndKDEDPhaseDone();
    void shellPhase();
    void shellPhaseDone();
    void autoStart0();
    void autoStart0Done();
    void autoStart1();
    void autoStart1Done();
    void autoStart2();
    void autoStart2Done();
    void restoreSession();
    void restoreSessionDone();
    void nextPhase(Phase phase = KDEInitPhaseDone);

    void logout();
    void logoutWithoutConfirmation();
    void haltWithoutConfirmation();
    void rebootWithoutConfirmation();

private:
    void configureShortcuts() const;
    QString printProcess(const QString &text = QString());
    QString time() const;

    QTime m_time;
    Phases m_phase;
    DBusInterface *m_dbusInterface;
    KDEDLoader *m_kdedLoader;
    ShortcutCollection *m_shortcutCollection;
    KSMServer *m_ksmServer;

    KLauncherInterface *m_klauncherInterface;
};

#endif // PKSESSION_H
